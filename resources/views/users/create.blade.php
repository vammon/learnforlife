@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Dashboard</div>

                <div class="panel-body">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach

                    {{ Form::open(array('url' => 'users')) }}
                        <p>
                            {{ Form::label('username', 'Username*') }}
                            {{ Form::text('username') }}
                        </p>
                        <p>
                            {{ Form::label('bio', 'Bio*') }}
                            {{ Form::text('bio') }}
                        </p>
                        <p>
                            {{ Form::label('password', 'Password*') }}
                            {{ Form::text('password') }}
                        </p>
                        <p>
                            {{ Form::label('password-repeat', 'Password-Repeat*') }}
                            {{ Form::text('password-repeat') }}
                        </p>
                        <p>
                            {{ Form::submit() }}
                        </p>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
