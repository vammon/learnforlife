<?php
/* CRUD METHODS ON MODELS */

/*
	Delete
*/
// Delete multiple entries that match a certain criteria
$deletion = Article::where('id', '<', 3);
$deletion->delete();
return 'Multiple Deletes';

// Delete a single entry
Article::destroy($id);
return 'Destroyed';

/*
	Update
*/
// update multiple entries that match a certain criteria
$articles = Article::where('id', '<', 3);
$articles->update([
	'body' => $new_value
])
return 'Mass Update';

// update a single article
$article = Article::find($id);
$article->title = $new_value;
$article->save();
return 'Saved New Title is: '.$article->title;

/*
	Read 
*/
// Find a entry
$article = Article::find($id);
return $article->title;

// find a set of entries
$articles = Article::where('db_column', '=|>|<', $var)->get();
foreach ($articles as $article) {
	var_dump($article->title);
}
return;

// get all entries
$articles = Article::all();
foreach ($articles as $article) {
	var_dump($article->title);
}
return

/*
	Create
*/
// create a article entry
$article = Article::create([
	'title' => $title
	]);

return $article->id;

$article = new article();
$article->title = $title;
$article->save();