<?php

/* https://www.youtube.com/watch?v=Zk9yuKJdQKM&list=PL1aVGMjc_xEjksIsnb5eagO-hGo_BzqY6&index=2
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('articles', function() {
    $articles = App\Article::all();

    echo "<ul>";
    foreach ($articles as $article) {
    	$comments = App\Article::find($article->id)->comments;
    	echo "<li>";
    	echo $article->title;
    	foreach ($comments as $comment) {
    		echo "<ul>";
    		echo "<li>". $comment->body ."</li>";
    		echo "</ul>";
    	}
    	echo "</li>";
    }
    echo "</ul>";
});
Auth::routes();

Route::resource('users', 'UsersController');

Route::get('/home', 'HomeController@index');
