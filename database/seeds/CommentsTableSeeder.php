<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 4; $i++) { 
        	Comment::create([
        		'body' => 'This is comment '.$i,
        		'article_id' => 1
        	]);
        }
        for ($i=4; $i < 6; $i++) { 
        	Comment::create([
        		'body' => 'This is comment '.$i,
        		'article_id' => 2
        	]);
        }
    }
}
