<?php

use Illuminate\Database\Seeder;
use App\Article;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CommentsTableSeeder::class);
    }
}

class ArticlesTableSeeder extends Seeder{
	public function run()
	{
		Article::create(array(
			'title' => 'How to ride a bike',
			'body' => '...'
		));
		Article::create(array(
			'title' => 'Potato peeling 4 beginners',
			'body' => '...'
		));
	}
}